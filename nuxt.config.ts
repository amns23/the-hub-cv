// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  modules: [
    'nuxt-primevue',
    'nuxt-calendly',
    'nuxt-security',
    'nuxt-purgecss',
    '@nuxt/image',
    'nuxt-delay-hydration'
  ],
  css: ['@/assets/css/main.scss'],
  purgecss: {
    enabled: true,
    safelist: ['pi-heart', 'pi-briefcase', 'p-accordion-content', 'p-galleria', 'p-highlight', 'router-link-active']
  },
  routeRules: {
    '/': { prerender: true },
  },
  app: {
    head: {
      htmlAttrs: {
        lang: 'en'
      },
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      link: [
        {
          id: 'theme-link',
          rel: 'stylesheet',
          href: '../themes/aura-dark-custom/theme.css',
        }
      ]
    }
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/css/responsive.scss";',
        },
      },
    },
  },
  target: 'server',
  build: {
    extractCSS: true,
    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    }
  },
  delayHydration: {
    // enables nuxt-delay-hydration in dev mode for testing
    debug: process.env.NODE_ENV === 'development',
    mode: 'mount'
  },
  security: {
    headers: {
      contentSecurityPolicy: {
        'frame-ancestors': false,
      }
    },
  },
  components: true,
})
